//NEXT: Have an array with wanted doc files. Verify if res.includes(docs) and count how many of them are there already i.e. 20% complete
//commit git clone/pull all repos scripts

//The way to run this script is through DevTools (Ctrl+Shift+i) > Sources > '>>' > Snippets > New snippet > copy paste the script there > run it (while in https://bitbucket.trimble.tools/projects/TECH)

//STEP 1: extracts the urls of all repos under TECH (or any other Bitbucket project really)
let array = document.getElementsByClassName("job-title");
let links = Array.from(array).map(x=>x.outerHTML.match(/<a[^>]+href=\"(.*?)\"[^>]*>(.*)?<\/a>/)[1]);
// console.log(links); // (25) ["/projects/TECH/repos/3dtilesunity/browse", "/projects/TECH/repos/aid2html/browse"...]
let repoNames = links.map(x=>x.split("/").slice(-2,-1)).flat(); //["3dtilesunity", "aid2html", "beaverton", ...]
console.log( repoNames)
links.forEach((x,idx)=>fetchEachRepo("https://bitbucket.trimble.tools"+x,repoNames[idx]));

//STEP 2: fetches the html of a given url and extracts repo file names 
function fetchEachRepo(url,repoName){
    let parser = new DOMParser();
    let htmlDoc;
    let res;
    fetch(url)
    .then(response => response.text())
    .then(data=>{
        htmlDoc = parser.parseFromString(data, 'text/html');
        res=Array.from(htmlDoc.getElementsByClassName("file file-row")).map(x=>x.dataset.itemName);
        res.unshift(repoName);
        console.log(res);
    })
    .catch((err)=>console.log(err));
}


//NOTES
// $('') is an alias for document.querySelector
// $$('') is an alias for document.querySelectorAll