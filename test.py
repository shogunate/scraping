## Declaring two variables in the same line
# arr,arr2=[],[] #multiple vars declared in the same line
# arr.append(1)
# arr2.append(2)
# print(arr)
# print(arr2)

# Sort lists & reverse order
# a = [3, 4, 1, 5, 2]
# b = a[::-1]
# print('Reversed a ->', b)
# s = sorted(a)
# r = s[::-1] # Reverse order
# print('Reversed s ->', r)

# Sort 2D list
list2d=[['Senior Security Engineer (Worldwide Remote)', '2021-07-06'], ['Talent Acquisition Manager (Worldwide Remote)', '2021-02-22'], ['Associate General Counsel (Worldwide Remote)', '2021-03-01'], ['Senior C# Developer (Worldwide Remote)', '2021-02-24'], ['Senior Android Engineer (Worldwide Remote)', '2021-03-01'], ['Senior iOS Engineer (Worldwide Remote)', '2021-03-01'], ['Senior macOS Engineer (Worldwide Remote)', '2021-03-01'], ['Director, Product Growth & Conversion (Worldwide Remote)', '2021-03-01'], ['Director, User Research (Worldwide Remote)', '2021-04-28'], ['Senior User Experience Researcher (Worldwide Remote)', '2021-04-28'], ['Senior Product Designer (Worldwide Remote)', '2021-02-26'], ['Senior Privacy Engineer (Worldwide Remote)', '2021-03-01']]
list2d.sort(key=lambda x: x[1])
rev=list2d[::-1]
print(rev[:2]) # show only the first 2 items