import requests, json, time
from bs4 import BeautifulSoup

res = requests.get("https://duckduckgo.recruitee.com/") # print(res) #200 # print(res.headers)
src=res.content 
soup = BeautifulSoup(src,'html.parser') #pip install html5lib (if html5 needed) # print(soup.prettify())

links,job_titles=[],[]
tags = soup.select("h5.job-title > a") # select all h5's with 'job-title' class and then take a's under it # print(headers) # print(isinstance(headers,list)) #True
for t in tags:
    links.append(t.attrs['href']) # for URLs 
    job_titles.append(t.contents)

for idx,l in enumerate(links):
    src2 = requests.get("https://duckduckgo.recruitee.com"+l).content
    soup = BeautifulSoup(src2,'html.parser')
    res = soup.find('script', type='application/ld+json').string #ALT: .contents[0]
    json_object = json.loads(res)
    job_titles[idx].append(json_object['datePosted'])    # print( "Iteration "+str(idx))
    time.sleep(0.5)

# [['Senior Security Engineer', '2021-07-06'], ['Talent Acquisition Manager', '2021-02-22']...
job_titles.sort(key=lambda x: x[1]) #sort by date 
rev=job_titles[::-1] #reverse the sort
print(rev[:2]) # show only the first 2 items

